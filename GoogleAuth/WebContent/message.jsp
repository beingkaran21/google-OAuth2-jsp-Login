<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Google OAuth Error Message</title>
</head>
<link href="social-buttons.css" rel="stylesheet"/>
<link href="bootstrap.min.css" rel="stylesheet">
<style>
.demoDiv {
  margin:auto;
  text-align:center;
}
</style>
<body>
<br/>
<div class="demoDiv">
<%String msg = (String)request.getAttribute("message"); %>
<h3><%=msg %></h3>
<a	href="/GoogleAuth/"
	class="btn btn-lg"> Go Back To Home Page 
</a>
</div>
</body>
</html>