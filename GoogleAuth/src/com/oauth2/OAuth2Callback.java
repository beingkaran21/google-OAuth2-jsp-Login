package com.oauth2;

import com.google.gson.Gson;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/oauth2callback")
public class OAuth2Callback
  extends HttpServlet
{
  private static final long serialVersionUID = 1L;
  static String className = "com.oauth2.Oauth2callback";
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    try
    {
      String code = request.getParameter("code");
      String urlParameters = "code=" + 
        code + 
        "&client_id=" + Setup.CLIENT_ID +
        "&client_secret=" + Setup.CLIENT_SECRET + 
        "&redirect_uri=" + Setup.REDIRECT_URL +
        "&grant_type=authorization_code";
  //    System.out.println(urlParameters);
      URL obj = new URL("https://accounts.google.com/o/oauth2/token?");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

      //URL url = new URL("https://accounts.google.com/o/oauth2/token?");
      //System.out.println(url.toString()+urlParameters);
      //URLConnection conn = url.openConnection();
      con.setDoOutput(true);
      OutputStreamWriter writer = new OutputStreamWriter(
        con.getOutputStream());
      writer.write(urlParameters);
      writer.flush();
    //  System.out.println(obj.toString());
      String line1 = "";
      BufferedReader reader = new BufferedReader(new InputStreamReader(
        con.getInputStream()));
      String line;
      while ((line = reader.readLine()) != null)
      {
        line1 = line1 + line;
      }
      String s = GsonUtility.getJsonElementString("access_token", line1);
      
      URL url = new URL(
        "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + 
        s);
      URLConnection conn = url.openConnection();
      line1 = "";
      reader = new BufferedReader(new InputStreamReader(
        conn.getInputStream()));
      while ((line = reader.readLine()) != null) {
        line1 = line1 + line;
      }
 /*     Scanner sc = new Scanner(url.openStream());
      String inline="";
	while(sc.hasNext())
      {
      inline+=sc.nextLine();
      }
      System.out.println("\nJSON data in string format");
      System.out.println(inline);
      sc.close();
*/
      GoogleDTO data = (GoogleDTO)new Gson().fromJson(line1, GoogleDTO.class);
      writer.close();
      reader.close();
      request.setAttribute("auth", data);
      request.getRequestDispatcher("/google.jsp").forward(request, response);
    }
    catch (MalformedURLException e)
    {
      e.printStackTrace();
    }
    catch (ProtocolException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {}
}
