<!DOCTYPE html>
<%@page import="com.oauth2.GlobalCons" %>
<%@page import="com.oauth2.GoogleDTO" %>
<head>
<title>Welcome To HomePage Via Oauth</title>
<link href='/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
</head>
<body>

<% 
	GoogleDTO gp = (GoogleDTO)request.getAttribute(GlobalCons.AUTH);
%>
<div style="width:400px;margin:auto;padding-top:30px;">
  <table class="table table-bordered">
    <tr>
      <td>User ID:</td>
      <td><%=gp.getId()%></td>
    </tr>
    <tr>
      <td>First Name:</td>
      <td><%=gp.getGiven_name()%></td>
    </tr>
    <tr>
      <td>Last Name:</td>
      <td><%=gp.getFamily_name()%></td>
    </tr>
    <tr>
      <td>Email:</td>
      <td><%=gp.getEmail()%></td>
    </tr>
    
    
  </table> 
</div>

</body>
</html>